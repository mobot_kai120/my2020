﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.IO;
using System.Threading;
using WebSocketSharp;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;

namespace _2020
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        public const uint A_SYNC = 0x434e5953;
        public const uint A_CNXN = 0x4e584e43;
        struct AdbMsgData
        {
            public uint command;     /* command identifier constant      */
            public uint arg0;        /* first argument                   */
            public uint arg1;        /* second argument                  */
            public uint data_length; /* length of payload (0 is allowed) */
            public uint data_check;  /* checksum of data payload         */
            public uint magic;       /* command ^ 0xffffffff             */
            public byte[] data;

        };
        public TcpClient clientSocket;
        private Thread _threadNetService;
        public NetworkStream serverStream;
        private bool ReadPacket(NetworkStream stream)
        {
            try
            {
                var reader = new BinaryReader(stream);
                //uint id = reader.ReadUInt32();
                //Console.Write("ID:" + id.ToString());
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("ReadPack" + ex.ToString());
            }
            return false;
        }
        private void ThreadNetService()
        {
            try
            {
                while (true)
                {
                    if (clientSocket != null && serverStream != null)
                    {
                        ReadPacket(serverStream);
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error" + ex.ToString());
                _threadNetService.Abort();
            }


        }
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var ws = new WebSocket("wss://socket.dworld.live/socket.io/?EIO=3&transport=websocket");
                ws.OnOpen += (senderWs, eSock) =>
                {
                    Console.WriteLine("Open");
                };
                ws.OnMessage += (senderWs, eSock) =>
                {
                    Console.WriteLine("OnMess" + eSock.Data);
                    int number = -1;
                    if(Int32.TryParse(eSock.Data, out number))
                    {
                        Console.WriteLine("sidaaa");
                        ws.Send("40/users,");
                    }

                };
                ws.OnError += (senderWs, eSock) =>
                {
                    Console.WriteLine("Erorr");
                };
                ws.OnClose += (senderWs, eSock) =>
                {
                    Console.WriteLine("Close");
                };
                ws.Connect();
                //    clientSocket = new TcpClient();
                //    clientSocket.Connect("127.0.0.1", 62001);//Nox 1
                //    serverStream = clientSocket.GetStream();

                //    MemoryStream memoryStream = new MemoryStream();
                //    BinaryWriter writer = new BinaryWriter(memoryStream);

                //    AdbMsgData msg;
                //    msg.command = A_CNXN;
                //    msg.arg0 = 0x01000000;
                //    msg.arg1 = 4 * 1024;
                //    msg.data_length = 7;
                //    msg.data_check = 0;

                //    msg.magic = msg.command ^ 0xffffffff;
                //    msg.data = Encoding.ASCII.GetBytes("host::");
                //    for (int i = 0; i < msg.data.Length; i++)
                //    {
                //         msg.data_check += msg.data[i];
                //    }
                //    Console.WriteLine("DataCheck"+ msg.data_check.ToString() + " " + msg.data.Length.ToString());

                //    writer.Write(msg.command);
                //    writer.Write(msg.arg0);
                //    writer.Write(msg.arg1);
                //    writer.Write(msg.data_length);
                //    writer.Write(msg.data_check);
                //    writer.Write(msg.magic);
                //    writer.Write(msg.data);

                //    if (msg.data_length > 0)
                //    {
                //        writer.Write(msg.data);
                //    }
                //    serverStream.Write(memoryStream.GetBuffer(), 0, (int)memoryStream.Length);
                //    _threadNetService = new Thread(new ThreadStart(ThreadNetService));
                //    _threadNetService.Start();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Click" + ex.ToString());
            }


            //AdbMsgHeader msg;
            //msg.command = 0x4e584e43;
            //msg.arg0 = 0x01000000;
            //msg.arg1 = 4 * 1024;
            //msg.data_length = 7;
            //byte[] data = 
            //serverStream.Write(msg,0,)
        }
        private void ThreadSelenium()
        {
            try
            {
                while (true)
                {

                    if (driver == null)
                    {
                        Console.WriteLine(AppDomain.CurrentDomain.BaseDirectory);
                        //Hide console
                        ChromeDriverService service = ChromeDriverService.CreateDefaultService();
                        service.HideCommandPromptWindow = true;
                        //Save cookies
                        ChromeOptions options = new ChromeOptions();
                        options.AddArguments(@"user-data-dir=" + AppDomain.CurrentDomain.BaseDirectory);
                        driver = new ChromeDriver(service, options);
                        //Goto
                        driver.Navigate().GoToUrl("https://wefinex.net/login");
                        UpdateStatus(lbStt, "Open Web");
                    }
                    else if (driver.Url != null && driver.Url.Equals("https://wefinex.net/index"))
                    {
                        //Bet
                        var isBet = driver.ExecuteScript("return window.localStorage.getItem(\"stateOpen\")");
                       // int numBeting = Int32.Parse(driver.ExecuteScript("return document.getElementsByClassName(\"totalCount text-uppercase\")[0].textContent").ToString());
                        //Console.WriteLine("numBeting " + numBeting);
                        if (isBet == null)
                        {
                            Console.WriteLine("isBet");
                            var a = driver.ExecuteScript("return document.getElementsByClassName(\"btnSuccess\")[0].getAttribute(\"disabled\")");
                            if (a == null)
                            {
                                var inputBet = driver.FindElementsById("InputNumber");
                                if (inputBet.Count <= 0 )
                                {
                                    inputBet = driver.FindElementsByXPath("//*[@id=\"betAmount\"]/div[1]/div[1]/div/input");
                                }
                                Console.WriteLine("inputBet"+ inputBet.Count);
                                if (inputBet.Count > 0)
                                {
                                    inputBet[0].SendKeys("5");
                                    driver.ExecuteScript("document.getElementsByClassName(\"btnSuccess\")[0].click()");
                                    UpdateStatus(lbStt, "Đánh bác ...");
                                }
                            }
                            else
                            {
                                Console.WriteLine("Tach");
                                UpdateStatus(lbStt,"Đang chờ ...");
                            }
                        }
                        else
                        {
                            UpdateStatus(lbStt,"Đã đánh");
                        }
                        IWebElement amount = driver.FindElementByXPath("//*[@id=\"rightNav\"]/ul/li[1]/div/a/div/div/span/b");
                        UpdateStatus(lb2, amount.Text);
                        //Get Chart
                        string iner = driver.ExecuteScript("return document.getElementsByClassName('highcharts-series highcharts-series-0 highcharts-candlestick-series highcharts-tracker')[0].innerHTML").ToString();
                        if (!string.IsNullOrEmpty(iner))
                        {
                            string myFilter = "fill=\"#([^\"]+)";
                            Regex ItemRegex = new Regex(myFilter, RegexOptions.Compiled);
                            MatchCollection lst = ItemRegex.Matches(iner);
                            int max = lst.Count;
                            int count = 0;
                            string txtLab = "";

                            foreach (Match ItemMatch in lst)
                            {
                                if (count < max - 1)
                                {
                                    string color = ItemMatch.Groups[1].ToString();
                                    if (color.Equals("31BAA0"))
                                    {
                                        txtLab = txtLab + "X";
                                    }
                                    else if (color.Equals("FC5F5F"))
                                    {
                                        txtLab = txtLab + "Đ";
                                    }
                                }
                                count++;
                            }
                            UpdateStatus(lb1, txtLab);
                        }
                    }
                    DateTime t = DateTime.Now;
                    int sec = t.TimeOfDay.Seconds;
                    int hours = t.TimeOfDay.Hours;
                    int min = t.TimeOfDay.Minutes;
                    //hh:mm:ss
                    Console.WriteLine("Time " + DateTime.Now.ToString("hh:mm:ss"));
                    Thread.Sleep(1000);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "error");
                _threadNetService.Abort();
            }


        }
        private ChromeDriver driver;
        private void button2_Click(object sender, EventArgs e)
        {
            
            try
            {
                _threadNetService = new Thread(new ThreadStart(ThreadSelenium));
                _threadNetService.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "error");
            }
           
        }
        private void UpdateStatus(Label lb,string text)
        {
            Action uiAction = () => lb.Text = text;
            lb.Invoke(uiAction);
        }
        private void Login()
        {
            IWebDriver driver = new ChromeDriver();
            //Goto
            driver.Navigate().GoToUrl("https://wefinex.net/login");
            //Login
            IWebElement emailInput = driver.FindElement(By.Name("email"));
            emailInput.SendKeys("clonephagamev1@gmail.com");

            IWebElement passInput = driver.FindElement(By.Name("password"));
            emailInput.SendKeys("123123");
        }
    }
}
